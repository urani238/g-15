#include<fstream>
#include <iostream>
#include<string>
#include<sstream>
#include <iomanip> 
using namespace std;
const char*filename1 = "F1.txt";//file chua da thuc thu nhat
const char*filename2 = "F2.txt";//file chua da thuc thu hai
const char*fileresult = "F-result.txt";//file chua cac da thuc ket qua

// class don thuc
template <class T>
class Monomial {
private:
	T exp;              //he so
	T BienX;
	int coef;           //so mu
	Monomial<T>* next;
public:
	//constructor
	Monomial();
	Monomial(T exp, T BienX, int coef);
	Monomial(T exp, int coef, Monomial<T>* m);
	//method
	//gan gia tri cho he so
	void setExp(T exp);
	//lay gia tri he so
	T getExp();
	//gan gia tri cho bien x
	void setBienX(T X);
	//lay gia tri bien x
	T getBienX();
	//gan gia tri so mu
	void setCoef(int coef);
	//lay gia tri so mu
	int getCoef();
	//lay don thuc tiep theo
	Monomial<T>* getNext();
	void setNext(Monomial<T>*);
	
	//xuat don thuc
	template <class U>
	friend istream& operator >> (istream& in, Monomial<U>& p);
	template <class U>
	friend ostream& operator << (ostream& out, const Monomial<U>& p);


};
//class da thuc
template <class T>
class Polynomial {
private:
	Monomial<T>* head;
	Monomial<T>* tail;
public:
	//constructor
	Polynomial<T>();

	//nhap da thuc
	void insert(char*);
	//rut gon da thuc
	Polynomial<T> reduction();
	//chuan hoa da thuc
	void sort();
	//cong da thuc
	Polynomial<T>& operator + (const Polynomial<T>&);
	//tru da thuc
	Polynomial<T>& operator - (const Polynomial<T>&);
	//nhan da thuc
	Polynomial<T>**operator * (const Polynomial<T>&);
	//in da thuc ra file
	void print(const char* filename);
	void AddTail(Monomial<T>*);
	Monomial<T>* getNode(T exp, T BienX, int coef);
	Monomial<T>* getHead();
	Monomial<T>* getTail();
};

template <class T>
Monomial<T>::Monomial() {
	exp = 0;
	coef = 0;
	BienX = 0;
	next = NULL;
}

template <class T>
Monomial<T>::Monomial(T exp, T X, int coef) {
	this->exp = exp;
	this->coef = coef;
	this->BienX = X;
	this->next = NULL;
}

template <class T>
Monomial<T>::Monomial(T exp, int coef, Monomial<T>* m) {
	this->exp = exp;
	this->coef = coef;
	this->next = m;
}

template <class T>
void Monomial<T>::setExp(T exp) {
	this->exp = exp;
}

template <class T>
T Monomial<T>::getExp() {
	return exp;
}

template <class T>
void Monomial<T>::setBienX(T X) {
	this->BienX = X;
}

template <class T>
T Monomial<T>::getBienX()
{
	return this->BienX;
}

template <class T>
void Monomial<T>::setCoef(int coef) {
	this->coef = coef;
}

template <class T>
int Monomial<T>::getCoef() {
	return this->coef;
}

template <class T>
Monomial<T>* Monomial<T>::getNext() {
	return this->next;
}
template <class U>
void Monomial<U>::setNext(Monomial<U>*mono)
{
	this->next = mono;
}
template <class U>
istream& operator >> (istream& in, Monomial<U>& p) {
	cout << "He so: ";
	in >> p.exp;
	cout << "Bien X: ";
	in >> p.BienX;
	cout << "So mu: ";
	in >> p.coef;

	return in;
}

template <class U>
ostream& operator << (ostream& out, const Monomial<U>& p) {
	out << p.exp << "*" << p.BienX << "^" << p.coef;
	return out;
}

//da thuc

//set toan bo da thuc voi gia tri 0
template<typename T>
Polynomial<T> setNull(Polynomial<T>& temp)
{
	Polynomial<T> poly;
	Monomial<T>*p = temp.getHead();
	string str("");
	while (p != NULL)
	{
		str += "0*0^0";
		p = p->getNext();
	}
	poly.insert((char*)str.c_str());
	return poly;
}
template<typename T>
void Polynomial<T>::AddTail(Monomial<T>*p)
{
	if (!head)
	{
		head = tail = p;
	}
	else
	{
		tail->setNext(p);
		tail = p;
	}
}
//tim ky tu ch[0], ch[1] trong chuoi str bai dau tim tu vi tri pos
int _find(char*str, char ch[2],int pos)
{
	for (int i = pos; i < strlen(str); i++)
	{
		if (str[i] == ch[0]|| (str[i] == ch[1]))
			return i; 
	}
	return -1;
}
//cat chuoi str tu vi tri start den vi tri end
char*supStr(char*str, int start, int end)
{
	char*temp = new char[end - start];
	int index = 0;
	for (int i = start; i < end; i++)
	{
		temp[index++] = str[i];
	}
	temp[index] = '\0';
	return temp;
}
// ham dua gia tri cua he so, so mu, bien x vao trong node
template<class T>
Monomial<T>*Polynomial<T>::getNode(T exp, T BienX, int coef)
{
	Monomial<T>*p=new Monomial<T>;
	if (!p)
		return NULL;
	p->setBienX(BienX);
	p->setCoef(coef);
	p->setExp(exp);
	p->setNext(NULL);
	return p;
}
//ham lai node o vi tri dau
template<class T>
Monomial<T>* Polynomial<T>::getHead()
{
	return this->head;
}
//ham lai node o vi tri cuoi
template<class T>
Monomial<T>* Polynomial<T>::getTail()
{
	return this->tail;
}
//ham khoi tao da thuc
template<class T>
Polynomial<T>::Polynomial()
{
	this->head = this->tail = NULL;
}
// chuyen 1 chuoi sang DSLK
template<class T>
void Polynomial<T>::insert(char *str)
{
	char ch[] = "-+";
	int posfind = 0;
	int pos = _find(str, ch, 0);
	char*result = NULL;
	char*pch1, *pch2;
	T exp,BienX;
	while (pos != -1)
	{
		result = supStr(str, posfind, pos);
		posfind += strlen(result) ;
		pch1 = strtok(result, "*");
		exp = atof(pch1);
		pch1 = strtok(NULL, "*");
		pch2 = strtok(pch1, "^");
		BienX = atof(pch2);
		pch2 = strtok(NULL, "^");
		int coef = 1;
		if (pch2)
			coef = atoi(pch2);
		Monomial<T>* mono = getNode(exp, BienX, coef);
		AddTail(mono);
		pos = _find(str, ch, pos+1);
	}
	result = supStr(str, posfind, strlen(str));
	pch1 = strtok(result, "*");
	exp = atof(pch1);
	pch1 = strtok(NULL, "*");
	pch2 = strtok(pch1, "^");
	 BienX = atof(pch2);
	pch2 = strtok(NULL, "^");
	int coef = 1;
	if (pch2)
		coef = atoi(pch2);
	Monomial<T>* mono = getNode(exp, BienX, coef);
	AddTail(mono);
}
//ham convert 1 so ki tu tempalte sang chuoi
template<class T>
string Convert(T number)
{
	stringstream stream;
	stream << fixed << setprecision(2) << number;
	string s = stream.str();
	return s;
}
//ham chuyen 1 da thuc trong DSLK sang chuoi de viet vao file
template<class T>
string ToString( Polynomial<T>&p)
{
	string str("");
	int count = 0;
	for (Monomial<T>*m = p.getHead(); m ; m = m->getNext())
	{
		if (count == 0)
		{
			if (m->getExp() == 0 || m->getBienX() == 0)
				continue;
			str += Convert(m->getExp())+"*"+ Convert(m->getBienX());
			if (m->getCoef() == 1)
			{
				count = 1;
				continue;
			}
			str += "^"+ to_string(m->getCoef());
		}
		else
		{
			if (m->getExp() > 0)
			{
				str+= "+";
			}
			if (m->getExp() == 0 || m->getBienX() == 0)
				continue;
			str += Convert(m->getExp()) + "*" + Convert(m->getBienX());
			if (m->getCoef() == 1)
				continue;
			str += "^" + to_string(m->getCoef());
		}
		count = 1;
	}
	return str;
}
//ham chuyen 2 da thuc p1,p2 trong DSLK sang chuoi de viet vao file voi dieu kien co ton tai hay khong ton tai da thuc p3
template<class T>
string ToString(Polynomial<T>&p1, Polynomial<T>&p2, Polynomial<T>&p3)
{
	Monomial<T>*p = p2.getHead();
	string str=ToString(p3);
	int count = 0;
	for (Monomial<T>*m = p1.getHead(); m; m = m->getNext())
	{
		if (m->getExp() > 0)
		{
			if (p3.getHead()&&count==0)
				str += "+";
			else if (count == 1)
				str+= "+";
			if (m->getExp() == 0 || m->getBienX() == 0)
			{
				count = 1;
				continue;
			}
			str += Convert(m->getExp());
			str+= "*"+ Convert( m->getBienX());
			if (m->getCoef() == 1)
			{
				count = 1;
				continue;
			}
			str += "^" +to_string( m->getCoef());
		}
		else
		{
			if (m->getExp() == 0 || m->getBienX() == 0)
			{
				count = 1;
				continue;
			}
			str +=Convert( m->getExp()) + "*" +Convert( m->getBienX());
			if (m->getCoef() == 1)
			{
				count = 1;
				continue;
			}
			str += "^" +to_string( m->getCoef());
		}
		if (p->getBienX() == 0)
		{
			count = 1;
			continue;
		}
		str += "*("+Convert( p->getBienX() );
		if (p->getCoef() != 1)
			str += "^" +to_string( p->getCoef())+")";
		else
			str += ")";
		p = p->getNext();
		count = 1;
	}
	return str;
}
//ham viet da thuc vao file
template<class T>
void Polynomial<T>::print(const char* filename)
{
	fstream f1,f2,f;
	f1.open(filename1, ios_base::in);
	if (!f1.is_open())
	{
		cout << "file F1.txt has not created!!!";
		return;
	}
	f2.open(filename2, ios_base::in);
	if (!f2.is_open())
	{
		cout << "file F2.txt has not created!!!";
		return;
	}
	f.open(fileresult, ios_base::out);
	string str1, str2;
	f1 >> str1;
	f2 >> str2;
	Polynomial<T> p, q, **k, w;
	f << "F1\n";
	if (str1 != "")
	{
		p.insert((char*)str1.c_str());
		p = p.reduction();
		p.sort();
		ToString(p);
		f << ToString(p) << endl;
	}
	f << "F2\n";
	if (str2 != "")
	{
		q.insert((char*)str2.c_str());
		q = q.reduction();
		q.sort();
		f << ToString(q) << endl;
	}
	if (str1 == ""&&str2 != "")
	{
		p = setNull(q);
	}
	if (str1 != ""&&str2 == "")
	{
		q = setNull(p);
	}
	w = p + q;
	w = w.reduction();
	w.sort();
	f << "F3\n" << ToString(w) << endl;
	w = p - q;
	w = w.reduction();
	w.sort();
	f << "F4\n" << ToString(w) << endl;
	f << "F5\n";
	if (str1 == "" || str2 == "")
	{
		k = p*q;
		if (k[0]->getHead())
		{
			*k[0] = k[0]->reduction();
			k[0]->sort();
		}
		if (k[1]->getHead())
			f << ToString(*k[1], *k[2], *k[0]) << endl;
	}
	else
		f << "0" << endl;
	f.close();
	f1.close();
	f2.close();
}
template<class T>
//rut gon da thuc
Polynomial<T>Polynomial<T>::reduction()
{
	Polynomial<T>temp;
	bool Check = true;
	for (Monomial<T>*m1 = head; m1; m1 = m1->getNext())
	{
		for (Monomial<T>*m3 = temp.head; m3; m3 = m3->getNext())
		{
			if (m3->getBienX() == m1->getBienX() && m3->getCoef() == m1->getCoef())
			{
				Check = false;
				break;
			}
		}
		if (m1->getBienX() == 0)
			continue;
		if (Check == true)
		{
			for (Monomial<T>*m2 = m1->getNext(); m2; m2 = m2->getNext())
			{
				if ( m2->getBienX()==0)
					continue;
				if (m1->getBienX() == m2->getBienX() && m1->getCoef() == m2->getCoef())
					m1->setExp(m1->getExp() + m2->getExp());
			}
			Monomial<T>* mono = getNode(m1->getExp(), m1->getBienX(), m1->getCoef());
			temp.AddTail(mono);
		}
		else
		{
			Check = true;
		}
	}
	temp.getTail()->setNext(NULL);
	return temp;
}
//ham chuyen doi 2 gia tri cua don thuc
template<class T>
void Swap(Monomial<T>*&m1, Monomial<T>*&m2)
{
	Monomial<T>*temp=new Monomial<T>;
	temp->setBienX(m1->getBienX());
	temp->setCoef(m1->getCoef());
	temp->setExp(m1->getExp());

	m1->setBienX(m2->getBienX());
	m1->setCoef(m2->getCoef());
	m1->setExp(m2->getExp());

	m2->setBienX(temp->getBienX());
	m2->setCoef(temp->getCoef());
	m2->setExp(temp->getExp());

}
template<class T>
//chuan hoa da thuc
void Polynomial<T>::sort()
{
	for (Monomial<T>*m1 = head; m1 ;  m1 = m1->getNext())
	{
		for (Monomial<T>*m2 = m1->getNext(); m2; m2 = m2->getNext())
		{
			if (m2->getCoef() < m1->getCoef())
			{
				Swap(m1, m2);
			}
			if (m2->getCoef() == m1->getCoef())
			{
				if (m2->getBienX() < m1->getBienX())
				{
					Swap(m1, m2);
				}
			}
		}
	}
	
}
template<class T>
bool Check(Polynomial<T>&Poly1,  Monomial<T>*p)
{
	for (Monomial<T>*q = Poly1.getHead(); q; q = q->getNext())
	{
		if (p->getBienX() == q->getBienX() && p->getCoef() == q->getCoef())
		{
			return false;
		}
	}
		return true;
}
//cong da thuc
template<class T>
Polynomial<T>& Polynomial<T>::operator + (const Polynomial<T>&addPoly)
{
	Polynomial<T>temp;
	for (Monomial<T>*p = head; p; p = p->getNext())
	{
		T Sum = p->getExp();
		for (Monomial<T>*q = addPoly.head; q; q = q->getNext())
		{
			if (p->getBienX() == q->getBienX() && p->getCoef() == q->getCoef())
			{
				Sum += q->getExp();
			}
		}
		Monomial<T>*mono = getNode(Sum, p->getBienX(), p->getCoef());
		temp.AddTail(mono);
	}
	for (Monomial<T>*p = addPoly.head; p; p = p->getNext())
	{
		if (Check(*this, p)==false)
		{
			continue;
		}
		Monomial<T>*mono = getNode(p->getExp(), p->getBienX(), p->getCoef());
		temp.AddTail(mono);
	}
	return temp;
}

//tru da thuc
template<class T>
Polynomial<T>& Polynomial<T>::operator - (const Polynomial<T>&MinusPoly)
{
	Polynomial<T>temp;
	for (Monomial<T>*p = head; p; p = p->getNext())
	{
		T Minus = p->getExp();
		for (Monomial<T>*q = MinusPoly.head; q; q = q->getNext())
		{
			if (p->getBienX() == q->getBienX() && p->getCoef() == q->getCoef())
			{
				Minus -= q->getExp();
			}
		}
		Monomial<T>*mono = getNode(Minus, p->getBienX(), p->getCoef());
		temp.AddTail(mono);
	}
	for (Monomial<T>*p = MinusPoly.head; p; p = p->getNext())
	{
		if (Check(*this, p) == false)
		{
			continue;
		}
		p->setExp(-p->getExp());
		Monomial<T>*mono = getNode(p->getExp(), p->getBienX(), p->getCoef());
		temp.AddTail(mono);
	}
	return temp;
}

//nhan da thuc
template<class T>
Polynomial<T>**Polynomial<T>::operator * (const Polynomial<T>&MultyPoly)
{
	Polynomial<T>**temp;
	temp = new Polynomial<T>*[3];
	temp[0] = new Polynomial<T>;
	temp[1] = new Polynomial<T>;
	temp[2] = new Polynomial<T>;
	int Coef = 0;
	T BienX = 1;
	bool isEx = true;
	for (Monomial<T>*p = head; p; p = p->getNext())
	{
		T Multy = p->getExp();
		for (Monomial<T>*q = MultyPoly.head; q; q = q->getNext())
		{
			Multy *= q->getExp();
			if (p->getBienX() == q->getBienX() && !(p->getCoef() == q->getCoef()))
			{
				 Coef = p->getCoef() + q->getCoef();
				 BienX = p->getBienX() ;
				 isEx = false;
			}
			else if (!(p->getBienX() == q->getBienX()) && p->getCoef() == q->getCoef())
			{
				Coef = p->getCoef();
				BienX = p->getBienX()*q->getBienX();
				isEx = false;
			}
			else if (!(p->getBienX() == q->getBienX()) && !(p->getCoef() == q->getCoef()))
			{
				isEx = true;
			}
			else
			{
				Coef = 2*p->getCoef();
				BienX = p->getBienX();
				isEx = false;
			}
			if (isEx == false)
			{
				Monomial<T>*mono = getNode(Multy, BienX, Coef);
				temp[0]->AddTail(mono);
			}
			else
			{
				Monomial<T>*mono1 = getNode(Multy, p->getBienX(), p->getCoef());
				temp[1]->AddTail(mono1);
				Monomial<T>*mono2 = getNode(1, q->getBienX(), q->getCoef());
				temp[2]->AddTail(mono2);
			}
			Multy = p->getExp();
		}
	}
	return temp;
}
int main() 
{
	Polynomial<float>p;
	p.print(fileresult);
	system("pause");
}
